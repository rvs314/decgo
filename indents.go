package dec

const TAB_SIZE = 4 // How many spaces a tab should be
const STD_INDENT = "    " // A standard indent (4 spaces)

// Given a line, give back its indent level and content
func parseLine(data string) (int, string) {
    runes := []rune(data)
    lvl := 0

    for i, r := range(runes) {
        switch r {
            case '\t':
                lvl += TAB_SIZE
            case ' ':
                lvl += 1
            default:
                return lvl, string(runes[i:])
        }
    }

    return 0, data
}

// Given a line, give back its indent level and content
func getIndent(data string) int {
    runes := []rune(data)
    lvl := 0

    for _, r := range(runes) {
        switch r {
            case '\t':
                lvl += TAB_SIZE
            case ' ':
                lvl += 1
            default:
                return lvl
        }
    }

    return 0
}

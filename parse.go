package dec

import (
    "strings"
    "regexp"
    "errors"
)

// Does a given line have anything in it?
func isEmpty(line string) bool {
    return strings.Trim(line, " \t") == ""
}

// Regular expression compiler for key-value pairs
var rgxKVP = regexp.MustCompile(`^\s*([^`+CHAR_REP+`\s]|\S.*[^`+CHAR_REP+`\s])\s*:\s*(.*)\s*$`)

// Is a given line a key-value pair?
func isKVP(line string) bool {
    return rgxKVP.Match([]byte(line))
}

// Given a censored line with a KVP, return if it was found, the key and the value
func parseKVP(line string) (string, string, error) {
    matches := rgxKVP.FindSubmatch([]byte(line))
    if len(matches) == 0 { return "", "", errors.New("Could not match to KVP") }
    return actual(string(matches[1])), actual(string(matches[2])), nil
}

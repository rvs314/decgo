package dec

import (
    "fmt"
    "strings"
)

// Warning - Globalism

var index int
var lines []string

// Base for recursion
// Side Effects: requires/modifies globals index and requires lines
func Unmarshall(data string) (map[string]interface{}, error) {
    index = 0
    lines = strings.Split(data, "\n")
    return parseKVPs()
}

// What KVPs are near 
// Side Effects: requires/modifies global index and requires lines
func parseKVPs() (map[string]interface{}, error) {

    values := make(map[string]interface{})

    if skipBlanks() { return values, nil }

    startIndLvl := getIndent(clean(lines[index]))

    // For each line,
    for ; index < len(lines); index++  {

        if skipBlanks() { return values, nil }

        // Parse/Clean the line
        indLvl, line := parseLine(lines[index])
        line = clean(line)

        // Ensure you're not on an indent level above the proper one
        if indLvl > startIndLvl { 
            return nil, fmt.Errorf("Found unexpected indent on line %d: \"%s\"", index+1, actual(line)) 
        }

        // If the current indent level is less than the one you started with
        if indLvl < startIndLvl {
            // You've gone too far, go back one and return
            index--
            return values, nil
        }

        // Parse this line's KVP
        key, readValue, err := parseKVP(line)
        if err != nil {
            return nil, fmt.Errorf("\"%s\" error on line %d: \"%s\"", err, index+1, actual(line))
        }

        var value interface{} // The actual value of the thing

        // If there's no value - move on to the next line
        if isEmpty(readValue) {
            index++
            if index == len(lines) {
                return nil, fmt.Errorf("Indent required after open colon on line %d: \"%s\"", index, actual(line))
            }

            if isKVP(lines[index]) { // If the next line is a valid KVP:
                // Set value to the map of keys to values
                value, err = parseKVPs()
                if err != nil { return values, err }
            } else if getIndent(lines[index]) > startIndLvl { // If the next line is indented
                // Set value to a string array
                value, err = parseArr()
                if err != nil { return values, err }
            } else { // Otherwise, return an error for not closing the colon
                return nil, fmt.Errorf("Indent required after open colon on line %d: \"%s\"", index, actual(line))
            }
        } else { // Othersize, the value is just the one on the line
            value = actual(readValue)
        }

        values[key] = value
    }

    return values, nil
}

// What is the value of the string array at the location index?
// Side Effects: requires/modified global index and requires lines
func parseArr() ([]string, error) {
    var values []string // The array of values

    skipBlanks()
    startIndLvl := getIndent(lines[index])

    // For each line
    for ; index < len(lines); index++ {

        // Parse/clean the line at $index
        indLvl, line := parseLine(lines[index])
        line = clean(line)

        // Ensure you're not on an indent level above the proper one
        if indLvl > startIndLvl { return nil, fmt.Errorf("Indent error on line %d: \"%s\"", index+1, actual(line)) }

        // If the next level is less than yours, you've gone too far, go back one and return
        if indLvl < startIndLvl {
            index--
            return values, nil
        }

        // Append the line to values
        values = append(values, actual(line))
        
        // Skip the next blank lines
        skipBlanks()
    }

    return values, nil
}

// Increment index from a point with only whitespace to one with data - return if you hit the end or not
// Side Effects: requires/modifies global index, requires lines
func skipBlanks() bool {
    for index < len(lines) {
        if isEmpty(clean(lines[index])) {
            index++
        } else { return false }
    }
    return true
}

package dec

import (
    "testing"
    "reflect"
    "strings"
)

func TestMarshall(t *testing.T) {
    passTests := map[string]map[string]interface{}{
        `αlphabet: αβξδ`:
            map[string]interface{}{`αlphabet`: `αβξδ`},
        `My Message: Hello World`:
            map[string]interface{}{"My Message": "Hello World"},
        `My Message: Hey Buddy:: I'm talking to you`:
            map[string]interface{}{`My Message`: `Hey Buddy: I'm talking to you`},

`Things I Like:
    Ice Cream
    Root Beer
    Computers
`:          map[string]interface{}{
                "Things I Like":
                    []string {
                        "Ice Cream",
                        "Root Beer",
                        "Computers",
                },
            },
`Jeff:
    Job: Exterminator
    Age: 45
    OS: Mac OSX
`:          map[string]interface{} {
                "Jeff":
                    map[string]interface{} {
                        "Job": "Exterminator",
                        "Age": "45",
                        "OS": "Mac OSX",
                    },
            },
    }
    for str, dec := range(passTests) {
        v, e := Marshall(dec)
        if e != nil {
            t.Errorf("Failed to Marshall %q: %q\n", str, e)
        } else if strings.TrimSpace(v) != strings.TrimSpace(str) {
            t.Errorf("Marshalling failed for %q, instead got %q\n", str, v)
        }
    }
}

func TestUnmarshall(t *testing.T) {
    failTests := []string{
    `
    ImproperList: This
        One
        Two
        Three
    `,}

    passTests := map[string]map[string]interface{}{
        "My Message: Hello World": 
            map[string]interface{}{"My Message": "Hello World"},
        "My Message: Hey Buddy:: I'm talking to you":
            map[string]interface{}{"My Message": "Hey Buddy: I'm talking to you"},
        "Example: ::::": {"Example": "::"},
        "My Message: Hello World # This is my message":
            map[string]interface{}{"My Message": "Hello World"},
        "## of Configuration Languages: infinite":
            map[string]interface{}{"# of Configuration Languages": "infinite"},
    `
    My Name: John Davis
    My Job: Exterminator`: map[string]interface{}{"My Name": "John Davis", "My Job": "Exterminator"},

    `
    Things I Like:
        Ice Cream
        Root Beer
        Computers

    Things I Dislike:
        Cactuses
        Paper Cuts
        Stack Overflow Errors
    `: map[string]interface{}{"Things I Like": []string{"Ice Cream", "Root Beer", "Computers"},
           "Things I Dislike": []string{"Cactuses", "Paper Cuts", "Stack Overflow Errors"}},

    `
    Michael:
        Age: 21
        Occupation: Plumber
        OS: Plan 9

    John:
        Age: 32
        Occupation: Popcorn Salesman
        OS: Linux
    `: map[string]interface{}{"Michael": map[string]interface{}{"Age": "21",
          "Occupation": "Plumber",
          "OS": "Plan 9"},
         "John":  map[string]interface{}{"Age": "32",
          "Occupation": "Popcorn Salesman",
          "OS": "Linux"}},

    `
    # Some places that exist
    Locations:
        White House:
            House Number: 1600
            Street: Pennsylvania Avenue
            Inhabitants: 
                The President
                The First Lady
        Google HQ:
            House Number: 1600
            Street: Amphitheater Parkway
            Inhabitants: 
                Rob Pike
                Sundar Pichai

    # Some kinds of food I enjoy
    Cuisines:
        French:
            From: France
            Notable: French Onion Soup
        Chinese:
            From: China
            Notable: Dumplings
        Thai:
            From: Thailand
            Notable: Pad Thai
    `: map[string]interface{}{
        "Locations": map[string]interface{}{
            "White House": map[string]interface{}{
                "House Number": "1600",
                "Street": "Pennsylvania Avenue",
                "Inhabitants": []string{"The President", "The First Lady"},
            },
            "Google HQ": map[string]interface{}{
                "House Number": "1600",
                "Street": "Amphitheater Parkway",
                "Inhabitants": []string{"Rob Pike", "Sundar Pichai"},
            },
        },
        "Cuisines": map[string]interface{}{
            "French": map[string]interface{}{
                "From": "France",
                "Notable": "French Onion Soup",
            },
            "Chinese": map[string]interface{}{
                "From": "China",
                "Notable": "Dumplings",
            },
            "Thai": map[string]interface{}{
                "From": "Thailand",
                "Notable": "Pad Thai",
            },
       },
    },

    `
    This: is data
    # This is not
    `: map[string]interface{}{"This": "is data"},

    `
    First Key: has a string after it
    Second Key: # Data here is invalid
        Has not one
        not two
        but three strings in it
    Third Key:
        Sub Key ##1: Has a string in it
        Sub Key ##2:
            Has a list
            Of strings`:
        map[string]interface{}{
        "First Key": "has a string after it",
        "Second Key": []string {
            "Has not one",
            "not two",
            "but three strings in it",
        },
        "Third Key": map[string]interface{}{
        "Sub Key #1": "Has a string in it",
            "Sub Key #2": []string{
            "Has a list",
            "Of strings",
        },
           },
       },
    }
    for str, dec := range(passTests) {
        v, e := Unmarshall(str)
        if e != nil {
            t.Errorf("Unmarshalling failed on tes%q", str)
        } else if !reflect.DeepEqual(v, dec) {
            t.Errorf("Expected %q but found %q\n", dec, v)
        }
    }

    for _, test := range(failTests) {
        v, e := Unmarshall(test)
        if e == nil {
            t.Errorf("Incorrect tes%qassed with output %q\n", test, v)
        }
    }
}

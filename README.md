# decgo

A [Golang](https://golang.org) implementation of the [Dumb Easy Configuration (DEC) language](https://gitlab.com/rvs314/DEC). The decgo software package creates the `dec` package, which can be used to convert data to/from DEC.

## Datatypes

DEC Data       | Go Data
---            | ---
string         | `string`
string list    | `[]string`
key-value pair | `map[string]interface{}`

An example is:

```go
func main() {
    data := map[string]interface{}{
        "One thing": "Another Thing",
        "A List": []string{
            "Of Many",
            "Different",
            "Strings",
        },
        "A Recursive": map[string]interface{}{
            "List": []string{
                "Here",
            },
        },
    }

    /* Maps To:
    One Thing: Another Thing
    A List:
        Of Many
        Different
        Strings
    A Recursive
        List:
            Here
    */
}
```

## Writing Data to DEC (Marshalling)

decgo provides a function called `Marshall` which, given a map of strings to `interface{}`, returns a string (the DEC content) and an error.

Here's an example of using `Marshall`:

```go
package main

import (
    "gitlab.com/rvs314/decgo"
    "fmt"
)

func main() {
    data := map[string]interface{}{
        "One thing": "Another Thing",
        "A List": []string{
            "Of Many",
            "Different",
            "Strings",
        },
        "A Recursive": map[string]interface{}{
            "List": []string{
                "Here",
            },
        },
    }

    DEC, err := dec.Marshall(data)
    
    if err != nil { 
        fmt.Printf("DEC: %s\n", err) 
    } else { 
        fmt.Println(DEC)
    }

    /* Prints:
    A List:
        Of Many
        Different
        Strings
    A Recursive:
        List:
            Here
    One thing: Another Thing
    */

    // Remember, golang maps are not assured to be in any particular order, so ordered data should be done in string arrays, which are guaranteed to be in order
}
```

## Writing data from DEC

decgo also provides a function called `Unmarshall`, which allows DEC to be turned into actual data.

Here's an example:

```go
package main

import (
    "gitlab.com/rvs314/decgo"
    "fmt"
)

func main() {
    DEC := `
A List:
    Of Many
    Different
    Strings
A Recursive:
    List:
        Here
One thing: Another Thing
    `

    data, err := dec.Unmarshall(DEC)

    if err != nil {
        fmt.Printf("DEC: %s\n", err)
    } else {
        fmt.Printf("%q\n", data)
    }

    /* Output (spacing added):
    map[
        "A List": [
            "Of Many" 
            "Different" 
            "Strings"
         ] 
        "A Recursive":map[
            "List":["Here"]
        ] 
        "One thing":"Another Thing"
    ]
    */
}
```

## TODO's

* Implement struct (un)?marshalling

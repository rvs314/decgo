package dec

import (
    "strings"
    "regexp"
)

// Constant to replace the ':' and '#' characters when escaped
const CHAR_REP = "\uFFFD"

// Takes a string with escaped characters and turns them into their represented constants
func escape(in string) string {
    in = strings.ReplaceAll(in, "::", CHAR_REP + ":")
    return strings.ReplaceAll(in, "##", CHAR_REP + "#")
}

// Regular expression compiler for removing comments
var rgxCmt = regexp.MustCompile(`([^`+CHAR_REP+`]#|^#).*$`)

// Removes comments from a cleaned string
func censor(in string) string {
    return rgxCmt.ReplaceAllLiteralString(in, "")
}

// Given a dirty line, return a commentless, escaped version
func clean(in string) string {
    return censor(escape(in))
}

// Takes a string with represented constants and turns them into their actual characters
func actual(in string) string {
    return strings.ReplaceAll(in, CHAR_REP, "")
}

// Given a strings, add the escaped versions of certian characters
func dirty(in string) string {
    in =  strings.ReplaceAll(in, "#", "##")
    return strings.ReplaceAll(in, ":", "::")
}

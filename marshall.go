package dec

import (
    "fmt"
    "strings"
)

func Marshall(data map[string]interface{}) (string, error) {
    out := ""
    for k, v := range(data) {
        out += fmt.Sprintf("%s:", k) // TODO: Replace concatenation if n gets big
        switch v.(type) {
            case string:
                out += fmt.Sprintf(" %s\n", dirty(v.(string)))
            case []string:
                out += "\n"
                for _, s := range(v.([]string)) {
                    out += fmt.Sprintf(STD_INDENT + "%s\n", dirty(s))
                }
            case map[string]interface{}:
                out += "\n"
                lines, err := Marshall(v.(map[string]interface{}))
                if err != nil {
                    return "", fmt.Errorf("Error on key \"%s\": %s", k, err)
                }

                for _, s := range(strings.Split(lines, "\n")) {
                    out += fmt.Sprintf(STD_INDENT + "%s\n", s)
                }
            default:
                return "", fmt.Errorf("Could not find a valid type for %T", v)
        }
    }
    if out == "" { return "", fmt.Errorf("Cannot create an empty DEC list") }

    return strings.Trim(out, "\n"), nil
}
